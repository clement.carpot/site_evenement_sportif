-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 27 mai 2022 à 18:51
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `sport17`
--

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
CREATE TABLE IF NOT EXISTS `evenement` (
  `IDevenement` int(11) NOT NULL AUTO_INCREMENT,
  `NomEvenement` varchar(30) NOT NULL,
  `LieuEvenement` varchar(30) NOT NULL,
  `TypeEvenement` varchar(30) NOT NULL,
  `DateEvenement` date NOT NULL,
  `MaximumParticipant` int(11) NOT NULL,
  PRIMARY KEY (`IDevenement`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`IDevenement`, `NomEvenement`, `LieuEvenement`, `TypeEvenement`, `DateEvenement`, `MaximumParticipant`) VALUES
(5, 'Tournoi de Basketball', 'Poitiers', 'Basketball', '2021-12-17', 10),
(12, 'Tournois de Volley', 'Poitiers', 'Volley-Ball', '2022-12-25', 600),
(13, 'Marathon', 'La Rochelle', 'Course Ã  pied', '2022-06-15', 500),
(14, 'Tournoi de Basket', 'Puilboreau', 'Basketball', '2022-06-26', 50),
(16, 'Marathon de Rochefort', 'Rochefort', 'Marathon', '2001-07-16', 80),
(17, 'Marathon de Royan', 'Royan', 'Course Ã  pied', '2022-10-06', 650),
(18, 'Tournoi de Foot', 'Saujon', 'Football', '2022-11-27', 50);

-- --------------------------------------------------------

--
-- Structure de la table `resultat`
--

DROP TABLE IF EXISTS `resultat`;
CREATE TABLE IF NOT EXISTS `resultat` (
  `id_resultat` int(11) NOT NULL AUTO_INCREMENT,
  `NomFichier` varchar(100) NOT NULL,
  `IDevenement` int(11) NOT NULL,
  PRIMARY KEY (`id_resultat`),
  KEY `IDevement` (`IDevenement`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `resultat`
--

INSERT INTO `resultat` (`id_resultat`, `NomFichier`, `IDevenement`) VALUES
(115, '00b2bde0c53759402882a90e9a4ba9fb.pdf', 14),
(117, '443c214689b7f698360ddda33e48934b.pdf', 18),
(118, '44af74aec4f069cb3b0499b6f1ce811d.pdf', 17),
(119, '6e14adb708c485e0a6ec1a6aec415a4b.pdf', 13);

-- --------------------------------------------------------

--
-- Structure de la table `statusevenementutilisateur`
--

DROP TABLE IF EXISTS `statusevenementutilisateur`;
CREATE TABLE IF NOT EXISTS `statusevenementutilisateur` (
  `IdStatut` int(11) NOT NULL AUTO_INCREMENT,
  `IDutilisateur` int(11) NOT NULL,
  `IDevenement` int(11) NOT NULL,
  `Statut` int(11) NOT NULL,
  PRIMARY KEY (`IdStatut`),
  KEY `IDutilisateur` (`IDutilisateur`),
  KEY `IDevenement` (`IDevenement`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `statusevenementutilisateur`
--

INSERT INTO `statusevenementutilisateur` (`IdStatut`, `IDutilisateur`, `IDevenement`, `Statut`) VALUES
(7, 28, 12, 1),
(8, 29, 13, 1),
(9, 29, 14, 1),
(13, 30, 17, 1),
(15, 30, 18, 1),
(17, 29, 5, 2),
(18, 29, 18, 2),
(20, 30, 12, 2);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `Nom` varchar(20) NOT NULL,
  `Prenom` varchar(20) NOT NULL,
  `DateNaissance` date NOT NULL,
  `Mail` varchar(30) NOT NULL,
  `Telephone` int(10) NOT NULL,
  `Sexe` tinyint(1) NOT NULL,
  `IDUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `MotDePasse` varchar(100) NOT NULL,
  PRIMARY KEY (`IDUtilisateur`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`Nom`, `Prenom`, `DateNaissance`, `Mail`, `Telephone`, `Sexe`, `IDUtilisateur`, `MotDePasse`) VALUES
('CARPOT', 'ClÃ©ment', '1998-01-24', 'caclement17@gmail.com', 679670335, 1, 28, '$2y$10$A25vBlxbGDNZi2aICUwzx.1e53toq5he2IW7yUmgP1Lp.XQSA81XS'),
('BAUBET', 'Xavier', '2001-02-23', 'test.test@test.fr', 606060606, 1, 29, '$2y$10$kI5nLO8cNure5VgZWTQH8uHG3B6EEuRwKZB6hK0pckjkeeCQD9Hq.'),
('Biro', 'Nathan', '2001-05-05', 'test@test.fr', 628084855, 1, 30, '$2y$10$g2XFa0jLz7PO7al0.oZrMeDw2zSC/slS6k4qA8mHOzwz.042pz/oS');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `resultat`
--
ALTER TABLE `resultat`
  ADD CONSTRAINT `resultat_ibfk_1` FOREIGN KEY (`IDevenement`) REFERENCES `evenement` (`IDevenement`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `statusevenementutilisateur`
--
ALTER TABLE `statusevenementutilisateur`
  ADD CONSTRAINT `IDevenement` FOREIGN KEY (`IDevenement`) REFERENCES `evenement` (`IDevenement`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `IDutilisateur` FOREIGN KEY (`IDutilisateur`) REFERENCES `utilisateur` (`IDUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
