<?php

    include("include/ConnexionBDD.php");

    // On vérifie si un fichier a été envoyé
    if(isset($_FILES["pdf"]) && $_FILES["pdf"]["error"] == 0) {
    //     // On a reçu le pdf
    //     echo "<pre>";
    //         var_dump($_FILES);
    //     echo "</pre>";

        // On procède aux vérifications
        // On vérifie l'extension ET le type MIME
        $allowed = ["pdf" => "application/pdf"];

        $filename = $_FILES["pdf"]["name"];
        $filetype = $_FILES["pdf"]["type"];
        $filesize = $_FILES["pdf"]["size"];

        $extension = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

        // On vérifie l'absence de l'extension dans les clés de $allowed ou l'absence du type MIME dans les valeurs
        if(!array_key_exists($extension, $allowed) || !in_array($filetype, $allowed)){
            // Ici soit l'extension soit le type est incorrect
            echo("Erreur: format de fichier incorrect");
            $location = "EvenementUser.php?msg=4";
            header("Location: ".$location);  
            exit;
        }

        // Ici le type est correct
        // On limite a 10Mo
        if($filesize >  1048576 * 1048576) {
            echo("Fichier trop volumineux");
        }

        // On génére un nom unique pour chaque fichier uplodé
        $newname = md5(uniqid());
        // On génére le chemin complet
        $nomFichier = "$newname.$extension";
        // $newfilename = __DIR__ . "/uploads/$newname.$extension";
        $newfilename = __DIR__ . "/uploads/$newname.$extension";
        
        if(!move_uploaded_file($_FILES["pdf"]["tmp_name"], $newfilename)){
            echo("L'upload a échoué");
        }

        
        $IDevenement = $_GET['IDEve'];


        //On vérifie s'il n'y a pas déjà un document résultat pour cette évènement
        $req_verif_resultat = "SELECT * FROM resultat WHERE IDevenement =" . $IDevenement;
        $res_verif_resultat = $bdd -> query($req_verif_resultat); 

        if($res_verif_resultat -> rowCount() > 0){

            
            $location = "EvenementUser.php?msg=2";
            header("Location: ".$location);  
            exit;

        }

        // On insert le document dans le cas contraire
        else{

            $Req_InsertResultat = "INSERT INTO resultat (NomFichier, IDevenement) VALUES ('$nomFichier', $IDevenement)";
            $insertResultats = $bdd->prepare($Req_InsertResultat);

            //var_dump($Req_InsertResultat);

            $insertResultats->bindParam(":newfilename", $nomFichier);
            $insertResultats->bindParam(":IDevenement", $_POST['IDevenement']);


            $insertResultats->execute();

            $location = "EvenementUser.php?msg=3";
            header("Location: ".$location);  
            exit;

            
        }
    }
?>