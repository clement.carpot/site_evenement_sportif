<!DOCTYPE html> 
<html>
<div>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="css/inscription.css">
        <title>Inscription</title>

        <?php  
            include('include/ConnexionBDD.php') 
        ?>

    </head>

    <body>
        
        <form action="InscriptionUserPost.php" method="POST">
            <section class="gradient-custom-3">
            <div class="mask d-flex align-items-center h-100">
                <div class="container h-100">
                    <div class="row d-flex justify-content-center align-items-center h-100">
                        <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                            <div class="card" style="border-radius: 15px;">
                                <div class="card-body p-5">
                                    <h2 class="text-uppercase text-center mb-5">Creation de compte</h2>
                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="form3Example1cg">Nom</label>
                                            <input type="text" id="form3Example1cg" class="form-control form-control-lg" name="nom" required/>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="form3Example1cg">Prénom</label>
                                            <input type="text" id="form3Example1cg" class="form-control form-control-lg" name="prenom" required/>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="form3Example1cg">Date de naissance</label>
                                            <input type="date" id="form3Example1cg" class="form-control form-control-lg" name="dateNaissance" required/>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="form3Example1cg">Sexe</label>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" id="sexeHomme" value="1" name="sexe" required>
                                                <label class="form-check-label" for="sexeHomme">Homme</label>
                                                <br>
                                                <input class="form-check-input" type="radio" id="sexeFemme" value="2" name="sexe" required>
                                                <label class="form-check-label" for="sexeFemme">Femme</label>
                                            </div>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="form3Example3cg">Email</label>
                                            <input type="email" id="form3Example3cg" class="form-control form-control-lg" name="mail" required/>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="form3Example1cg">Téléphone</label>
                                            <input type="text" id="form3Example1cg" class="form-control form-control-lg" name="tel" min="10" max="11" required/>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="form3Example4cg">Mot de passe</label>
                                            <input type="password" id="form3Example4cg" class="form-control form-control-lg" name="password" required/>
                                        </div>

                                        <div class="form-outline mb-4">
                                            <label class="form-label" for="form3Example4cdg">Confirmation du mot de passe</label>
                                            <input type="password" id="form3Example4cdg" class="form-control form-control-lg" name="password2" required/>
                                        </div>

                                        <div class="d-flex justify-content-center">
                                            <button type="submit" class="btn btn-success btn-block btn-lg gradient-custom-4 text-body">S'inscrire</button>
                                        </div>

                                        <p class="text-center text-muted mt-5 mb-0">Vous avez déja un compte ? <a href="connexionUser.php" class="fw-bold text-body"><u>Se connecter</u></a></p>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </form>
    </body>
</html>