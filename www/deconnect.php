<?php 

// on détruit la session
session_start();
// On vite $_SESSION
$_SESSION = array();

session_unset();
session_destroy();

// on détruit le cookie
setcookie("id_client", '', 0);
setcookie("PHPSESSID", '', 0);
setcookie("auth", "", 0);

header("Location: ConnexionUser.php");

?>
