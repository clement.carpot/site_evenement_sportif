package com.example.projet;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.NameValuePair;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    EditText Nom,Prenom,pwd;
    Button btnValidez,button7,Inscription,Boutoninscrip;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Nom=(EditText) findViewById(R.id.Nom);
        Prenom=(EditText) findViewById(R.id.Prenom);
        pwd=(EditText) findViewById(R.id.pwd);
        btnValidez= (Button) findViewById(R.id.btnValidez);
        button7 = (Button) findViewById(R.id.button7);
        Boutoninscrip=(Button) findViewById(R.id.button5);

        button7.setOnClickListener(new View.OnClickListener() {

            public void onClick(View button7) {

                Intent intent1 = new Intent(MainActivity.this, InscriptionEvenementActivity.class);

                startActivity(intent1);
            }

        });
        Boutoninscrip.setOnClickListener(new View.OnClickListener() {

            public void onClick(View button7) {

                Intent intent2 = new Intent(MainActivity.this, CreationEve.class);

                startActivity(intent2);
            }

        });
        //Button btnStyle = (Button) findViewById(R.id.InscriptionEv);
        //btnStyle.setOnClickListener(new OnClickListener());
    }
        public void process(View v){
        String N1=Nom.getText().toString();
        String N2=Prenom.getText().toString();
        String N3=pwd.getText().toString();
        String qrystring="?t1="+N1+"¨&t2"+N2+"&t3"+N3;
        }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.inscription:
                Toast.makeText(this,"ouverture fenêtre Ouvrir !", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, InscriptionActivity.class);
                startActivity(intent);
                return true;
            case R.id.connexion:
                Toast.makeText(getApplicationContext(),"ouverture fenêtre Enregistrer !", Toast.LENGTH_LONG).show();
                Intent intent2 = new Intent(MainActivity.this, ConnexionActivity.class);
                startActivity(intent2);
                return true;
        }
        return false;
    }

}