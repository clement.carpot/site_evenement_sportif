package com.example.projet;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.NameValuePair;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.message.BasicNameValuePair;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

import android.widget.LinearLayout;


public class InscriptionEvenementActivity extends AppCompatActivity {
    EditText Edit;
    ListView ls;
    String LieuEve,NomEve,DateEve,TypeEve;
    String result=null;
    Button BouttonAffiche;
    String connstr="http://192.168.56.1/Eve/site_evenement_sportif/AndroidConnect/Select_Evenement.php";
    InputStream is = null;
    JSONObject json_data=null;
    /*for(int i=0;i<col1.length;i++){

    }*/

    String[] ValeurList = null;

    ListView mListView;
    /*String[] prenoms = new String[]{
            "Antoine", "Benoit", "Cyril", "David", "Eloise", "Florent",
            "Gerard", "Hugo", "Ingrid", "Jonathan", "Kevin", "Logan",
            "Mathieu", "Noemie", "Olivia", "Philippe", "Quentin", "Romain","Sophie",
            "Tristan", "Ulric", "Vincent", "Willy", "Xavier","Yann", "Zoé"

};*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription_evenement);
        BouttonAffiche = (Button) findViewById(R.id.AffichezEve);
        mListView = (ListView) findViewById(R.id.listview);
        Edit = (EditText) findViewById(R.id.Email);
        //ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        //nameValuePairs.add(new BasicNameValuePair("ville","L"));

        BouttonAffiche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Affichage bg = new Affichage(InscriptionEvenementActivity.this);
                bg.execute();
/*
                for (int i = 0; i < obj.length(); i++) {
                    String Valeur = "";

                    String NomEvenement = settings.getJSONObject(i).getString("NomEvenement");
                    String LieuEvenement = settings.getJSONObject(i).getString("LieuEvenement");
                    String TypeEvenement = settings.getJSONObject(i).getString("TypeEvenement");
                    String DateEvenement = settings.getJSONObject(i).getString("DateEvenement");

                    Valeur = "Nom Evenement : " + NomEvenement + "Lieu Evenement : " + LieuEvenement
                            + "Type Evenement : " + LieuEvenement + "Date Evenement : " + DateEvenement;

                    ValeurList[i] = Valeur;


                }*/


            }
        });
/*
        ArrayAdapter<String> adapter = new ArrayAdapter<>(InscriptionEvenementActivity.this,
                android.R.layout.simple_list_item_1, Collections.singletonList(result));
        mListView.setAdapter(adapter);
*/

/*
        for (int i = 0; i < result.length(); i++) {
            String Valeur = "";

            String NomEvenement = result.getJSONObject(i).getString("NomEvenement");
            String LieuEvenement = result.getJSONObject(i).getString("LieuEvenement");
            String TypeEvenement = result.getJSONObject(i).getString("TypeEvenement");
            String DateEvenement = result.getJSONObject(i).getString("DateEvenement");

            Valeur = "Nom Evenement : " + NomEvenement + "Lieu Evenement : " + LieuEvenement
                    + "Type Evenement : " + LieuEvenement + "Date Evenement : " + DateEvenement;

            ValeurList[i] = Valeur;

        
    }*/
    }



    private class Affichage extends AsyncTask<String, Void, String>{
        AlertDialog Dialog;
        Context context;
        public Affichage(Context context){this.context = context;}




        @Override
        public void onPreExecute(){
            super.onPreExecute();
            Dialog = new AlertDialog.Builder(context).create();
            Dialog.setTitle("Etat de connexion");
        }

        @Override
        protected  String doInBackground(String... strings)  {
            //String Nom = strings[0];// déclaration de la variable (ici c'est le nom du produit)
            //String Prenom = strings[1]; //déclaration de la varaibele (ici c'est le prix du produit)
            //String Mdp = strings[2];
            String result = "";
            String connstr = "http://192.168.56.1/Eve/site_evenement_sportif/AndroidConnect/Select_Evenement.php";
            //Permet d'afficher les evenement dans l'ordre alphabetic

            //mListView.setAdapter(nameValuePairs);

            // Envoie de la commande http
            try {
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);
                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
                StringBuilder sb = new StringBuilder();

                String ligne = "";
                while ((ligne = reader.readLine()) != null) {
                    sb.append(ligne + "\n"); }

                result=sb.toString();


                JSONArray jARRAY= new JSONArray(result);
                for( int i=0;i<= jARRAY.length();i++){
                    JSONObject json_data=jARRAY.getJSONObject(i);

                    Log.i("log_tag","NomEvenement"+json_data.getString("NomEvenement")+
                            "LieuEvenement"+json_data.getString("LieuEvenement")+
                            "TypeEvenement"+json_data.getString("TypeEvenement")+
                            ",DateEvenement: "+json_data.getString("DateEvenement"));
                }
                reader.close();
                ips.close();
                http.disconnect();

                return result;



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(String s) { //execute l'insertion et affiche success si tout c'est bien passé
            super.onPostExecute(s);//execute

            Dialog.setMessage(s);
            Dialog.show();

        }
    }
}