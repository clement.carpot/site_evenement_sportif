package com.example.projet;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class InscriptionActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        EditText Nom,Prenom,Mdp,Mail,numeroTel,DateNais,Mdp2;
        Button btnValidez;


        Nom = (EditText) findViewById(R.id.Nom);
        Prenom = (EditText) findViewById(R.id.Prenom);
        Mdp = (EditText) findViewById(R.id.pwd);
        Mail= (EditText) findViewById(R.id.Email);
        numeroTel= (EditText) findViewById(R.id.Phone);
        Mdp2= (EditText) findViewById(R.id.PwdConfirme);


        DateNais= (EditText) findViewById(R.id.DateNais);



        btnValidez =(Button) findViewById(R.id.btnValidez);



        btnValidez.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String des= Nom.getText().toString();
                String pre= Prenom.getText().toString();
                String mdp= Mdp.getText().toString();
                String mail= Mail.getText().toString();
                String num= numeroTel.getText().toString();
                String date= DateNais.getText().toString();
                String ConfirmMdp= Mdp2.getText().toString();



                insert_produit bg = new insert_produit(InscriptionActivity.this) ;
                bg.execute(des,pre,mdp,mail,num,date,ConfirmMdp);
                //numclientp
                //DateNaisClientp
            }
        });


    }
    private class insert_produit extends AsyncTask<String, Void, String>{
        AlertDialog Dialog;
        Context context;
        public insert_produit(Context context){this.context = context;}

        @Override
        public void onPreExecute(){
            super.onPreExecute();
            Dialog = new AlertDialog.Builder(context).create();
            Dialog.setTitle("Etat de connexion");
        }
        protected String doInBackground(String ... strings){
            String result = "";
            String nom= strings[0];
            String pren=strings[1];
            String motdepasse= strings[2];
            String email=strings[3];
            String numclient=strings[4];
            String datenais=strings[5];
            String ConfirmMdp=strings[6];

            //lien vers le fichier php
            String connstr = "http://sio.fenelon-notredame.fr:4480/sport17/AndroidConnect/InsertUser.php";
            try {
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);
                OutputStream ops = http.getOutputStream();
                BufferedWriter writer= new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
                String data = URLEncoder.encode("nom","UTF-8")+"="+URLEncoder.encode(nom,"UTF-8")+
                        "&&" +URLEncoder.encode("prenom","UTF-8")+"="+URLEncoder.encode(pren,"UTF-8")+
                        "&&" +URLEncoder.encode("motdepasse","UTF-8")+"="+URLEncoder.encode(motdepasse,"UTF-8")+
                        "&&" +URLEncoder.encode("MailUti","UTF-8")+"="+URLEncoder.encode(email,"UTF-8")+
                        "&&" +URLEncoder.encode("TelUti","UTF-8")+"="+URLEncoder.encode(numclient,"UTF-8")+
                        "&&" +URLEncoder.encode("DateUti","UTF-8")+"="+URLEncoder.encode(datenais,"UTF-8")+
                        "&&" +URLEncoder.encode("ConfirmMdp","UTF-8")+"="+URLEncoder.encode(ConfirmMdp,"UTF-8");
                writer.write(data);
                Log.v("InscriptionActivity", data);
                writer.flush();
                writer.close();
                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
                String ligne = "";
                while ((ligne = reader.readLine()) != null) {
                    result += ligne;
                }
                reader.close();
                ips.close();
                http.disconnect();


                return result;


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;

        }
        @Override
        protected void onPostExecute(String s) { //execute l'insertion et affiche success si tout c'est bien passé
            super.onPostExecute(s);//execute

            Dialog.setMessage(s);
            Dialog.show();
            if (s.contains("succes insertion")) {
                Toast.makeText(context, "Produit inséré avec succès.", Toast.LENGTH_LONG).show();
                Intent intent3 = new Intent(InscriptionActivity.this, MainActivity.class);
                startActivity(intent3);
            } else {
                Toast.makeText(context, "Problème d'insertion.", Toast.LENGTH_LONG).show();
            }
        }
    }
}

////result = result+ "\n\t" + jARRAY.getJSONObject(i);










