package com.example.projet;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.HttpClient;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.methods.HttpPost;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class ConnexionActivity extends AppCompatActivity {
    EditText identif,mdp;
    Button BouttonExe,InscripBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        identif= (EditText) findViewById(R.id.Identif);
        mdp= (EditText) findViewById(R.id.Mdp);
        BouttonExe= (Button) findViewById(R.id.ConnectClie);
        InscripBtn= (Button) findViewById(R.id.InscriptionBtn);


        BouttonExe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mail= identif.getText().toString();
                String Mdp = mdp.getText().toString();


                Identifiant bg = new Identifiant(ConnexionActivity.this);
                bg.execute(mail,Mdp);


                /*Intent intent2 = new Intent(ConnexionActivity.this, MainActivity.class);
                startActivity(intent2);*/



            }
        });

        InscripBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View button7) {

                Intent intent2 = new Intent(ConnexionActivity.this, InscriptionActivity.class);

                startActivity(intent2);
            }

        });

    }
    private class Identifiant extends AsyncTask<String, Void, String>{
        AlertDialog Dialog;
        Context c;
        public Identifiant (Context context){this.c=context; }



        @Override
        public void onPreExecute(){
            super.onPreExecute();
            Dialog = new AlertDialog.Builder(c).create();
            Dialog.setTitle("Etat de connexion");
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            String Mail = strings[0];
            String Mdp = strings[1];
            String connstr = "http://sio.fenelon-notredame.fr:4480/sport17/AndroidConnect/VerifConnexion.php";
            try {
                URL url = new URL(connstr);
                HttpURLConnection http = (HttpURLConnection) url.openConnection();
                http.setRequestMethod("POST");
                http.setDoInput(true);
                http.setDoOutput(true);
                OutputStream ops = http.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(ops, "UTF-8"));
                String data = URLEncoder.encode("mail", "UTF-8") + "=" + URLEncoder.encode(Mail, "UTF-8") +
                        "&&" + URLEncoder.encode("pwd", "UTF-8") + "=" + URLEncoder.encode(Mdp, "UTF-8");
                writer.write(data);
                writer.flush();
                writer.close();
                InputStream ips = http.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "ISO-8859-1"));
                String ligne = "";
                while ((ligne = reader.readLine()) != null) {
                    result = result+ligne;
                }
                reader.close();
                ips.close();
                http.disconnect();
                return result;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("error",e.getMessage());
            }
            return result;

        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Dialog.setMessage(s);
            Dialog.show();
            if (s.contains("succes")){
                Intent intent2 = new Intent(ConnexionActivity.this, MainActivity.class);

                startActivity(intent2);
            }

        }



    }
}




