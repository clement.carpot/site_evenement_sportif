package com.example.projet;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class CreationEve extends AppCompatActivity {







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creation_eve);
        Button CreaEve;
        EditText Nomevenement,LieuEve,TypeEve,DateEve,MaxiJoueur;


        Nomevenement = (EditText) findViewById(R.id.NomEvenement);
        TypeEve = (EditText) findViewById(R.id.TypeEvenement);
        LieuEve = (EditText) findViewById(R.id.LieuEvenement);
        MaxiJoueur = (EditText) findViewById(R.id.MaxClient);
        DateEve = (EditText) findViewById(R.id.DateEvenement);
        CreaEve= (Button) findViewById(R.id.Envoi2);
        CreaEve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String NomEve= Nomevenement.getText().toString();
                String Type= TypeEve.getText().toString();
                String LieuEv= LieuEve.getText().toString();
                String MaxiJou= MaxiJoueur.getText().toString();
                String Date= DateEve.getText().toString();


                CreationEve.insert_Eve bg = new CreationEve.insert_Eve(CreationEve.this);
                bg.execute(NomEve,LieuEv,Type,Date,MaxiJou);

            }
        });
    }

        private class insert_Eve extends AsyncTask<String, Void, String> {
            AlertDialog Dialog;
            Context context;
            public insert_Eve(Context context){this.context = context;}

            @Override
            public void onPreExecute(){
                super.onPreExecute();
                Dialog = new AlertDialog.Builder(context).create();
                Dialog.setTitle("Etat de connexion");
            }

            protected String doInBackground(String ... strings){
                String result = "";
                String des= strings[0];
                String pre=strings[1];
                String Typeeve= strings[2];
                String dateeve=strings[3];
                String maxijoeur=strings[4];
                String connstr = "http://sio.fenelon-notredame.fr:4480/sport17/AndroidConnect/InsertEvenement.php";
                try {
                    URL url = new URL(connstr);
                    HttpURLConnection http = (HttpURLConnection) url.openConnection();
                    http.setRequestMethod("POST");
                    http.setDoInput(true);
                    http.setDoOutput(true);
                    OutputStream ops = http.getOutputStream();
                    BufferedWriter writer= new BufferedWriter(new OutputStreamWriter(ops,"UTF-8"));
                    String data = URLEncoder.encode("nomEve","UTF-8")+"="+URLEncoder.encode(des,"UTF-8")+
                            "&&" +URLEncoder.encode("LieuEve","UTF-8")+"="+URLEncoder.encode(pre,"UTF-8")+
                            "&&" +URLEncoder.encode("TypeEve","UTF-8")+"="+URLEncoder.encode(Typeeve,"UTF-8")+
                            "&&" +URLEncoder.encode("MaxiPart","UTF-8")+"="+URLEncoder.encode(maxijoeur,"UTF-8")+
                            "&&" +URLEncoder.encode("DateEve","UTF-8")+"="+URLEncoder.encode(dateeve,"UTF-8");
                    writer.write(data);
                    Log.v("InscriptionActivity", data);
                    writer.flush();
                    writer.close();
                    InputStream ips = http.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(ips, "UTF-8"));
                    String ligne = "";
                    while ((ligne = reader.readLine()) != null) {
                        result += ligne;
                    }
                    reader.close();
                    ips.close();
                    http.disconnect();

                    return result;


                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return result;
            }
            @Override
            protected void onPostExecute(String s) { //execute l'insertion et affiche success si tout c'est bien passé
                super.onPostExecute(s);//execute

                Dialog.setMessage(s);
                Dialog.show();
                if (s.contains("succes insertion")) {
                    Toast.makeText(context, "Produit inséré avec succès.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Problème d'insertion.", Toast.LENGTH_LONG).show();
                }
            }
        }
}