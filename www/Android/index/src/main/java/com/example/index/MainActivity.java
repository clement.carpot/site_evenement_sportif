package com.example.index;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.Inscription:
                //Toast.makeText(getApplicationContext(),"ouverture fenêtre Ouvrir !", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, inscription.class);
                startActivity(intent);
                return true;
            case R.id.Connexion:
                //Toast.makeText(getApplicationContext(),"ouverture fenêtre Enregistrer !", Toast.LENGTH_LONG).show();
                //Intent intent2 = new Intent(MainActivity.this, vuesecondaire1.class);
                //startActivity(intent2);
                return true;
        }
        return false;
    }

}