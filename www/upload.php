<!DOCTYPE HTML>
<html lang="fr">
    <head>
        <meta charset="utf-8">
        <title>Test envoi fichier</title>
        <script src="javascript/envoiResultats.js"></script>
        <script>
            var nomFichier = <?php echo json_encode($nomFichier); ?>;
            var cheminPDF = "uploads/"+nomFichier;
            function voirResultat() {
                window.open(cheminPDF,"blank");
            }
        </script>
    </head>

    <body>
        <!--  -->
        <form method="POST" enctype="multipart/form-data" action="Post_ResultatEvenement.php">
            <div>
                <input type="file" name="pdf" id="fichier" accept=".pdf">
                <br><br>
                <input type="text" name="IDevenement" value = <?php echo("'" . $info_evenement['IDevenement'] . "'");?> hidden>
                <input name="submitEvent" id="submit" type="submit" value="Envoyer les résultats" onclick="envoiResultats()" hidden>
                <label for="submit" class="uploadfile">Envoyer les résultats</label>
                <label for="resultats" class="voirResultats">Voir les résultats</label>
                <button id="resultats" onclick=voirResultat() hidden></button>
                <br><br>
            </div>
        </form>
    </body>
</html>