<?php

include("include/ConnexionBDD.php");

include("include/fonctions.php");

// GESTION DE LA SESSION
include("include/session_cookie.php");

?>

<!DOCTYPE html>
<html lang="fr">
<head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Liste Evenement </title>
    <link rel="stylesheet" href="css/LISTEevenements.css">
    <link rel="stylesheet" href="css/style.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">


</head>

<body>

    <?php
        include("include/EnTete.php");
        $months = array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');
        $i = 0;
    ?>

<br><br>

    <div class="container">
        <div class="row">
            <div class="col-lg-10 mx-auto mb-4">
                <div class="section-title text-center ">
                    <h2 class="top-c-sep"> Liste des évènements : </h2>
                </div>
            </div>
        </div>

        
        <div class="row">
                <div class="col-lg-10 mx-auto">
                    <div class="career-search mb-60">

                        <!-- Espace de recherche -->
                        <form action="ListeEvenement_post.php?Post=true" class="career-form mb-60" method="POST">
                            <div class="row">
                                <div class="col-md my-3">

                                    <select class="form-select" aria-label="Default select example" name="TypeEvenement">

                                    <?php

                                        $req_type_evenement = $bdd->query('SELECT TypeEvenement FROM evenement GROUP BY TypeEvenement');

                                        ?>

                                            <option value='' selected> Recherche par sport </option>

                                        <?php

                                        while ($donnees_type = $req_type_evenement->fetch())
                                        {
                                            ?>     
                                                <option value="<?php echo $donnees_type['TypeEvenement']; ?>"> <?php echo $donnees_type['TypeEvenement']; ?></option>

                                            <?php
                                        }       

                                    ?>

                                    </select>

                                </div>

                                <div class="col-md my-3">

                                    <select class="form-select" aria-label="Default select example" name="LieuEvenement">

                                    <?php

                                        $req_lieu_evenement = $bdd->query('SELECT LieuEvenement FROM evenement GROUP BY LieuEvenement ORDER BY LieuEvenement');

                                        ?>

                                            <option value='' selected> Recherche par ville </option>

                                        <?php

                                        while ($donnees_type = $req_lieu_evenement->fetch())
                                        {

                                            ?> 

                                                <option value="<?php echo $donnees_type['LieuEvenement']; ?>"> <?php echo $donnees_type['LieuEvenement']; ?></option>

                                            <?php

                                        }     

                                    ?>

                                    </select>

                                </div>

                                <div class="col-md my-3">
                                    <button type="submit" class="btn btn-primary"> Rechercher </button>
                                </div>
                            
                        </form>

                        <form action="ListeEvenement_post.php?Post=true2" class="career-form mb-60" method="POST">
                                <div class="col-md my-3">
                                    <button type="submit" class="btn btn-primary"> Afficher tout les évènements </button>
                                </div>
                        </form>
                        </div>
                        <!-- Fin espace de recherche -->


                        <div class="filter-result align-item center">
                            <br>
                            <div class='job-box'>
                            <div class='row'>

                            <?php

                                if(isset($_GET['L']) && $_GET['L'] != "" && isset($_GET['T']) && $_GET['T'] != ""){

                                    $Lieu = str_replace("%20", " ", $_GET['L']);
                                    $Type = str_replace("%20", " ", $_GET['T']);

                                    $req_evenement = "SELECT * FROM evenement WHERE LieuEvenement = '". $Lieu ."' AND TypeEvenement = '". $Type ."' ORDER BY DateEvenement";
                                    $info_sql = $bdd->query($req_evenement);

                                    while ($info_evenement = $info_sql->fetch()){

                                        $Date = explode('-',$info_evenement['DateEvenement']);
                                        $Annee = $Date[0];
                                        $Mois = $months[$Date[1]-1];
                                        $Jour = $Date[2];
                                        $url_inscription = "InscriptionEvenement.php?ID=" . $info_evenement['IDevenement'];
                                
                            ?>

                                    <div class="card" style="width: 18rem;">
                                        <!-- <img class="card-img-top" src="Image/natation1.png" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title"> <?php echo($info_evenement['NomEvenement']); ?> </h5><br>
                                            <p class="card-text"> <?php echo(" <u>Date</u> : ". $Jour . " " . $Mois . " " . $Annee . "."); ?> </p>
                                            <p class="card-text"> <?php echo(" <u>Lieu</u> : ". $info_evenement['LieuEvenement'] . "."); ?> </p>
                                            <p class="card-text"> <?php echo(" <u>Sport</u> : ". $info_evenement['TypeEvenement'] . "."); ?> </p>
                                            <a href= <?php echo("'". $url_inscription ."'"); ?> class="btn btn-primary">S'inscrire</a>
                                        </div>
                                    </div>

                            <?php
                            
                                        $i += 1;
                                        $div = $i/3;
                                        if(is_int ($div)){
                                            echo("</div><div class='row'>");
                                        }

                                    }
                                }

                                elseif(isset($_GET['L']) && $_GET['L'] != "" && !isset($_GET['T'])){

                                    $Lieu = str_replace("%20", " ", $_GET['L']);

                                    $req_evenement = "SELECT * FROM evenement WHERE LieuEvenement = '". $Lieu ."' ORDER BY DateEvenement";
                                    $info_sql = $bdd->query($req_evenement);

                                    while ($info_evenement = $info_sql->fetch()){

                                        $Date = explode('-',$info_evenement['DateEvenement']);
                                        $Annee = $Date[0];
                                        $Mois = $months[$Date[1]-1];
                                        $Jour = $Date[2];
                                        $url_inscription = "InscriptionEvenement.php?ID=" . $info_evenement['IDevenement'];
                                
                            ?>

                                    <div class="card" style="width: 18rem;">
                                        <!-- <img class="card-img-top" src="Image/natation1.png" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title"> <?php echo($info_evenement['NomEvenement']); ?> </h5><br>
                                            <p class="card-text"> <?php echo(" <u>Date</u> : ". $Jour . " " . $Mois . " " . $Annee . "."); ?> </p>
                                            <p class="card-text"> <?php echo(" <u>Lieu</u> : ". $info_evenement['LieuEvenement'] . "."); ?> </p>
                                            <p class="card-text"> <?php echo(" <u>Sport</u> : ". $info_evenement['TypeEvenement'] . "."); ?> </p>
                                            <a href= <?php echo("'". $url_inscription ."'"); ?> class="btn btn-primary">S'inscrire</a>
                                        </div>
                                    </div>

                            <?php

                                        $i += 1;
                                        $div = $i/3;
                                        if(is_int ($div)){
                                            echo("</div><div class='row'>");
                                        }

                                    }
                                }

                                elseif(isset($_GET['T']) && $_GET['T'] != "" && !isset($_GET['L'])){

                                    $Type = str_replace("%20", " ", $_GET['T']);

                                    $req_evenement = "SELECT * FROM evenement WHERE TypeEvenement = '". $Type ."' ORDER BY DateEvenement";
                                    $info_sql = $bdd->query($req_evenement);

                                    while ($info_evenement = $info_sql->fetch()){

                                        $Date = explode('-',$info_evenement['DateEvenement']);
                                        $Annee = $Date[0];
                                        $Mois = $months[$Date[1]-1];
                                        $Jour = $Date[2];
                                        $url_inscription = "InscriptionEvenement.php?ID=" . $info_evenement['IDevenement'];
                                
                            ?>

                                    <div class="card" style="width: 18rem;">
                                        <!-- <img class="card-img-top" src="Image/natation1.png" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title"> <?php echo($info_evenement['NomEvenement']); ?> </h5><br>
                                            <p class="card-text"> <?php echo(" <u>Date</u> : ". $Jour . " " . $Mois . " " . $Annee . "."); ?> </p>
                                            <p class="card-text"> <?php echo(" <u>Lieu</u> : ". $info_evenement['LieuEvenement'] . "."); ?> </p>
                                            <p class="card-text"> <?php echo(" <u>Sport</u> : ". $info_evenement['TypeEvenement'] . "."); ?> </p>
                                            <a href= <?php echo("'". $url_inscription ."'"); ?> class="btn btn-primary">S'inscrire</a>
                                        </div>
                                    </div>

                            <?php

                                        $i += 1;
                                        $div = $i/3;
                                        if(is_int ($div)){
                                            echo("</div><div class='row'>");
                                        }
                                    }
                                }

                                else{
 
                                    $req_evenement = "SELECT * FROM evenement ORDER BY DateEvenement";
                                    $info_sql = $bdd->query($req_evenement);

                                    while ($info_evenement = $info_sql->fetch()){

                                        $Date = explode('-',$info_evenement['DateEvenement']);
                                        $Annee = $Date[0];
                                        $Mois = $months[$Date[1]-1];
                                        $Jour = $Date[2];
                                        $url_inscription = "InscriptionEvenement.php?ID=" . $info_evenement['IDevenement'];
                                
                            ?>

                                    <div class="card" style="width: 18rem;">
                                        <!-- <img class="card-img-top" src="Image/natation1.png" alt="Card image cap"> -->
                                        <div class="card-body">
                                            <h5 class="card-title"> <?php echo($info_evenement['NomEvenement']); ?> </h5><br>
                                            <p class="card-text"> <?php echo(" <u>Date</u> : ". $Jour . " " . $Mois . " " . $Annee . "."); ?> </p>
                                            <p class="card-text"> <?php echo(" <u>Lieu</u> : ". $info_evenement['LieuEvenement'] . "."); ?> </p>
                                            <p class="card-text"> <?php echo(" <u>Sport</u> : ". $info_evenement['TypeEvenement'] . "."); ?> </p>
                                            <a href= <?php echo("'". $url_inscription ."'"); ?> class="btn btn-primary">S'inscrire</a>
                                        </div>
                                    </div>

                            <?php

                                        $i++;
                                        $div = $i/3;
                                        if(is_int ($div)){
                                            echo("</div> <div class='w-100'></div> <div class='row'>");
                                        }

                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>