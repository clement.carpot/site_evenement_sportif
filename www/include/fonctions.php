<?php

function uploader_fichier($fichier_upload, $dossier_dest)
{
	//upload
	$dossier     = $dossier_dest;
	$fichier     = basename($fichier_upload['name']);
	$taille_maxi = 80000000;
	
	//var_dump($fichier_upload['tmp_name']);
	$taille     = filesize($fichier_upload['tmp_name']);
	$extensions = array('.png', '.pdf', '.PNG', '.PDF', '.jpg', '.JPG', '.xls', '.xlsx', '.XLS', '.XLSX', '.csv', '.CSV');
	$extension  = strrchr($fichier_upload['name'], '.'); 
    $erreur = '';
	// test 
	// var_dump($fichier);
	// var_dump($dossier);
	// var_dump($taille);
	// var_dump($extension);

	// Début des vérifications de sécurité...
	if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
	{
	    $erreur .= '<div class="alert alert-danger">Vous devez uploader un fichier avec les extensions possibles suivante :
	      png, gif, jpg, jpeg ou bien pdf.<br /></div>';
	    //var_dump('problème extension');
	}
	if($taille > $taille_maxi)
	{
	    $erreur .= '<div class="alert alert-danger">Le fichier est trop volumineux !.<br /></div>';
	    //var_dump('trop volumineux');
	}
	if($erreur == '') // S'il n'y a pas d'erreur, on upload
	{
	    //On formate le nom du fichier ici...
	    // transformer les caractères accentués en entités HTML
	    $fichier = htmlentities($fichier, ENT_NOQUOTES, 'utf-8');
	    // remplacer les entités HTML pour avoir juste le premier caractères non accentués
	    // Exemple : "&ecute;" => "e", "&Ecute;" => "E", "à" => "a" ...
	    $fichier = preg_replace('#&([A-za-z])(?:acute|grave|cedil|circ|orn|ring|slash|th|tilde|uml);#', '\1', $fichier);
	    // Remplacer les ligatures tel que : , Æ ...
	    // Exemple "œ" => "oe"
	    $fichier = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $fichier);
	    // Supprimer tout le reste
	    $fichier = preg_replace('#&[^;]+;#', '', $fichier);

	     if(move_uploaded_file($fichier_upload['tmp_name'], $dossier . $fichier)) 
	     //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
	     {
	     	//var_dump('ok');
	     	// ne sert pas mais peut être utile à l'avenir
	        $piece_jointe = $dossier.$fichier;
	     }
	     else //Sinon (la fonction renvoie FALSE).
	     {
	        $erreur .= '<div class="alert alert-danger">Le fichier est trop volumineux ! Il doit être inférieur à '.$taille_maxi_texte.'.<br /></div>';
	     }
	}
}

function get_Info_User($id_user)
{
	global $connexionPDO;
	
	if (! isset($id_user) || $id_user == '' || $id_user == 0)
		return false;

	$req_info_user = "SELECT * FROM utilisateur";
	$req_info_user .= " WHERE IDUtilisateur =".$id_user;

	//test affichage
	//var_dump($req_info_user);

    $res_info_user = $connexionPDO->query($req_info_user);

	return $user = $res_info_user->fetch(PDO::FETCH_ASSOC);
}

function get_Info_Site_Client($id_client)
{
	global $connexionPDO;
	
	if (! isset($id_client) || $id_client == '' || $id_client == 0)
		return false;

	$req_info_site_client = "SELECT * FROM sites";
	$req_info_site_client .= " WHERE id_client =".$id_client;

	//test affichage
	//var_dump($req_info_site_client);

    $res_info_site_client = $connexionPDO->query($req_info_site_client);

	return $sites = $res_info_site_client->fetchAll(PDO::FETCH_ASSOC);
}




function add_Message($type, $id_user, $destinataires, $dateheure_aujourdhui, $num_chantier, $texte_message, $objet_message, $vu_id_user, $id_type_message, $etat_message, $nb_reponse, $sstype = '', $date_sinistre = '0000-00-00', $estimation_sinistre = 0, $description_sinistre = '', $type_sinistre = NULL)
{
    global $connexionPDO;
    global $dernier_msg;

    $req_insert_msg = "INSERT INTO messages_divers (type, expediteur, destinataire, date_expedition, num_chantier, texte_message, objet_message, vu_id_user, id_type_msg, etat_message, nb_reponse, sstype, date_sinistre, estimation_sinistre, description_sinistre";
    if(is_null($type_sinistre))
        $req_insert_msg .= ")";
    else
        $req_insert_msg .= ", type_sinistre)";
    $req_insert_msg .= " VALUES ('".$type."', '".$id_user."', '".$destinataires."', '".$dateheure_aujourdhui."', '".$num_chantier."', '".$texte_message."', '".$objet_message."', '".$vu_id_user."', ".$id_type_message.", ".$etat_message.", ".$nb_reponse.", '".$sstype."', '".$date_sinistre."', ".$estimation_sinistre.", '".$description_sinistre."'";
    if(!is_null($type_sinistre))
        $req_insert_msg .= ", ".$type_sinistre.")";
    else
        $req_insert_msg .= ")";

    $res_insert_msg = $connexionPDO->exec($req_insert_msg);

    $dernier_msg = $connexionPDO->lastInsertId();

    return $dernier_msg;
}

function affiche_fonction_groupe_user($id_user)
{
	global $connexionPDO;

	$req_user = "SELECT * FROM utilisateur WHERE ID_Utilisateur =".$id_user;
    $res_user = $connexionPDO->query($req_user);

    $user = $res_user->fetch(PDO::FETCH_ASSOC);
  
    return $user['groupe_user'];
}

function derniere_connexion_user($mail_user)
{
	global $connexionPDO;

	$req_user_co = "SELECT * FROM connexions";
	$req_user_co .= " WHERE mail_user ='".$mail_user."' ORDER BY date_login DESC";
    $res_user_co = $connexionPDO->query($req_user_co);

    $user_co = $res_user_co->fetch(PDO::FETCH_ASSOC);
  
    if($res_user_co->rowCount() > 0)
        return 'le '.ecritDate2($user_co['date_login'], true);
    else
        return 'Première connexion';
}

function get_utilisateur($id_user = 'tous')
{
	global $connexionPDO;

	$req_user = "SELECT * FROM utilisateur";
	if($id_user != 'tous')
		$req_user .= " WHERE IDUtilisateur =".$id_user;
    $res_user = $connexionPDO->query($req_user);
    $tab_user = $res_user->fetchAll();

    return $tab_user;
}

function affiche_user_fonction($id_user = 'tous', $fonction1, $fonction2 = false)
{
	global $connexionPDO;

	$req_user = "SELECT * FROM utilisateur";
    if($fonction2 != false && $fonction2 != '')
	   $req_user .= " WHERE (fonction_user ='".$fonction1."' OR fonction_user ='".$fonction2."')";
    else
       $req_user .= " WHERE fonction_user ='".$fonction1."'";
	if($id_user != 'tous')
		$req_user .= " AND IDUtilisateur =".$id_user;
    $res_user = $connexionPDO->query($req_user);

    $tab_user_sav = $res_user->fetchAll();
  
    return $tab_user_sav;
}

function affiche_user_groupe($id_user = 'tous', $groupe1, $groupe2 = false)
{
    global $connexionPDO;

    $req_user = "SELECT * FROM utilisateur";
    if($groupe2 != false && $groupe2 != '')
       $req_user .= " WHERE (groupe_user ='".$groupe1."' OR groupe_user ='".$groupe2."')";
    else
       $req_user .= " WHERE groupe_user ='".$groupe1."'";
    if($id_user != 'tous')
        $req_user .= " AND IDUtilisateur =".$id_user;
    $req_user .= " ORDER BY nom_user ASC, prenom_user ASC";
    $res_user = $connexionPDO->query($req_user);

    $tab_user_sav = $res_user->fetchAll();
    //var_dump($req_user);
    return $tab_user_sav;
}


?>