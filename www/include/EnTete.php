<div class="row justify-content-between" id="navbarAcceuil">
    <?php
        if(isset($_SESSION['auth']) && $_SESSION['auth']==true){
            echo("                  
                <div class='col'>
                    <span class='navbar-text'>  &nbsp &nbsp Bonjour, vous êtes connecté en tant que : ". $_SESSION['infos_user']['Nom'] ." ". $_SESSION['infos_user']['Prenom'] ." ! </span>
                </div>");
                
                ?>
                    <div class="col-4" id="divBoutonNavbar">
                        <a class="btn btn-outline-success" type="button" href="connexionUser.php" id="BoutonConnexionAcceuil1">Changer de compte</a>
                        <a class="btn btn-outline-success" type="button" href="EvenementUser.php" id="BoutonConnexionAcceuil1">Liste de vos évènements</a>
                    

                <?php

                $Fichier_actuel = $_SERVER['SCRIPT_NAME'];

                if($Fichier_actuel != "/Projet sport17/site_evenement_sportif/www/index.php"){
                    ?>
                    <a class="btn btn-outline-success" type="button" href="index.php" id="BoutonConnexionAcceuil1">Accueil</a>
                    
                    <?php
                }
                echo("</div>"); 

        } else {
            echo("                  
                <div class='col'>
                    <span class='navbar-text'> Bonjour, vous n'êtes actuellement pas connecté.  </span>
                </div>");

                ?>
                    <div class="col-4" id="divBoutonNavbar">
                        <a class="btn btn-outline-success" type="button" href="connexionUser.php" id="BoutonConnexionAcceuil1">Connexion</a>
                        <a class="btn btn-outline-secondary" type="button" href="InscriptionUser.php" id="BoutonConnexionAcceuil2">Inscription</a>
                    </div>

                <?php 
        }
    ?>
</div>