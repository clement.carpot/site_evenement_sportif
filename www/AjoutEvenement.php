
<?php

include("include/ConnexionBDD.php");

include("include/fonctions.php");

// GESTION DE LA SESSION
include("include/session_cookie.php");

?>

<!DOCTYPE html>
<html>
    <head>

        <title>Ajout evenement</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">

        <?php

            include('include/ConnexionBDD.php')
            // $mysqli = mysqli_connect("localhost", "root", "", "sport17");
            // if (mysqli_connect_errno()) {
            //     echo "Echec lors de la connexion à MySQL : " . mysqli_connect_error();
            // }

        ?>
    </head>

    <body>

        <?php

            include("include/EnTete.php");

            if(isset($_GET['msg']) && $_GET['msg'] == 1) { echo("<br><br> L'évènement a bien été ajouté."); }

            if(isset($_GET['msg']) && $_GET['msg'] == 2) { echo("<br><br> L'évènement n'a pas été ajouté, vous avez déjà créé cet évènement."); }

            if(isset($_GET['msg']) && $_GET['msg'] == 3) { echo("<p class='alert alert-warning'> Le mot de passe est incorect. </p>"); } 

        ?>

            <center>
                
                <br><h2>Création d'un évènement : </h2><br>

                <form method="post" action="Post_CreaEvenement.php">

                <table>
                    <tr>
                        <td>
                            <div class="form-group event">
                                <label for="nom">Nom de l'évènement :</label>
                                <input class="form-control" type="text" name="NomEvenement" id="NomEvenement" placeholder="Nom de l'événement">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group event">
                                <label for="date">Date de l'évènement :</label>
                                <input class="form-control" type="date" name="DateEvenement" id="DateEvenement" placeholder="Date de l'événement">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group event">
                                <label for="lieu">Lieu de l'évènement :</label>
                                <input class="form-control" type="text" name="LieuEvenement" id="LieuEvenement" placeholder="Lieu de l'événement">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group event">
                                <label for="lieu">Type d'évènement :</label>
                                <input class="form-control" type="text" name="TypeEvenement" id="TypeEvenement" placeholder="Type d'événement"> 
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group event">
                                <label for="participant">Nombre maximum de participants :</label>
                                <input class="form-control" type="text" name="participant" id="participant" placeholder="Nombre de participants"> 
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td>                        
                            <div class="form-group event">
                            <label > <br><br> &nbsp;<b> Enregistrer l'évènement avec un compte utilisateur qui deviendra l'organisateur </b> &nbsp; <br></label>
                        </td>
                    </tr>

                    <tr>
                        <td>
                        <div class="form-group event">
                        <label for="nom"> &nbsp; Nom :</label>
                        <input required type="text" class="form-control" placeholder="Nom" name="Nom" id="Nom" value= <?php if(isset($_COOKIE['auth']) && $_COOKIE['auth'] == 1){ echo("'".$_COOKIE['infos_user']['Nom']."'"); } ?> >
                        </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                        <div class="form-group event">
                        <label for="prenom"> &nbsp; Prenom :</label>
                        <input required type="text" class="form-control" placeholder="Prenom" name="Prenom" id="Prenom" value= <?php if(isset($_COOKIE['auth']) && $_COOKIE['auth'] == 1){ echo("'".$_COOKIE['infos_user']['Prenom']."'"); } ?> >
                        </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                        <div class="form-group event">
                        <label for="mail"> &nbsp; Mail :</label>
                        <input required type="mail" class="form-control" placeholder="Mail" name="Mail" id="Mail" value= <?php if(isset($_COOKIE['auth']) && $_COOKIE['auth'] == 1){ echo("'".$_COOKIE['infos_user']['Mail']."'");} ?> >
                        </div>
                        </td>
                    </tr>

                    <tr>
                        <td>
                        <div class="form-group event">
                        <label for="password"> &nbsp; Mot de passe :</label>
                        <input type="password" class="form-control" placeholder="Mot de passe" name="Password" id="Password" required>
                        </div>
                        </td>
                    </tr>

                    <tr><td></td></tr>

                    <tr>
                        <td>
                            <div class="form-group">
                                <input class="form-control btn btn-primary center" type="submit" value="Envoyer">
                            </div>
                        </td>
                    </tr>

                </table>
            </center>
        </form>
    </body>
</html>
