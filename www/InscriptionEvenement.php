<?php

include("include/ConnexionBDD.php");

include("include/fonctions.php");

// GESTION DE LA SESSION
include("include/session_cookie.php");

?>

<!DOCTYPE html>
<html>
    <head>

        <title>Confirmation inscription evenement</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">

    </head>

    <body>

        <?php

            //var_dump($_COOKIE);
           // $req_info_evenement = $bdd->query('SELECT LieuEvenement FROM evenement GROUP BY LieuEvenement ORDER BY LieuEvenement');

           $months = array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');

           
    
           if(isset($_GET['ID'])){

                $ActionPost = "Post_InscriptionEvenement.php?ID=". $_GET['ID'];

                $req_infos_evenement = $bdd->query('SELECT * FROM evenement WHERE IDevenement = ' . $_GET['ID']);

                $donnees_evenement = $req_infos_evenement->fetch();

                $Date = explode('-',$donnees_evenement['DateEvenement']);
                    $Annee = $Date[0];
                    $Mois = $months[$Date[1]-1];
                    $Jour = $Date[2];

           }

           include("include/EnTete.php");

           if(isset($_GET['msg']) && $_GET['msg'] == 1) { echo("<br><br> Vous êtes bien inscrit à l'évènement."); }

           if(isset($_GET['msg']) && $_GET['msg'] == 2) { echo("<br><br> Vous êtes déjà inscrit à l'évènement."); }

           if (isset($_GET['ID'])) {
            echo("<h2>Inscription à un événement : </h2><br><p> &nbsp <u><b> Récapitulation des informations de l'évènement :</b></u> </p>" . "<p> &nbsp Nom de l'évènement : ". $donnees_evenement['NomEvenement']."</p>". "<p> &nbsp Lieu de l'évènement : ". $donnees_evenement['LieuEvenement']."</p>"
                . "<p> &nbsp Type de l'évènement : ". $donnees_evenement['TypeEvenement']."</p>". "<p> &nbsp Date de l'évènement : ". $Jour . " " . $Mois . " " . $Annee ."</p>".
                "<br><br> <p> &nbsp <u><b>Confirmer l'inscription avec les données suivantes :</b></u> </p>");

           if(isset($_COOKIE['auth']) && $_COOKIE['auth'] == 1){

        ?>

        
        <?php


            

        ?>
            

        <form method="post" action= <?php echo("'". $ActionPost ."'"); ?> >
            <?php if(isset($_GET['msg']) && $_GET['msg'] == 3) { echo("<p class='alert alert-warning'> Le mot de passe est incorect. </p>"); } ?>
            <table>

                <tr>
                    <td>
                    <label for="nom"> &nbsp Nom :</label>
                    </td>
                    <td>
                    <input type="text" name="Nom" id="Nom" value= <?php if(isset($_COOKIE['auth']) && $_COOKIE['auth'] == 1){ echo("'".$_COOKIE['infos_user']['Nom']."'"); } ?> required>
                    </td>
                </tr>

                <tr>
                    <td>
                    <label for="prenom"> &nbsp Prenom :</label>
                    </td>
                    <td>
                    <input type="text" name="Prenom" id="Prenom" value= <?php if(isset($_COOKIE['auth']) && $_COOKIE['auth'] == 1){ echo("'".$_COOKIE['infos_user']['Prenom']."'"); } ?>  required>
                    </td>
                </tr>

                <tr>
                    <td>
                    <label for="mail"> &nbsp Mail :</label>
                    </td>
                    <td>
                    <input type="mail" name="mail" id="mail" value= <?php if(isset($_COOKIE['auth']) && $_COOKIE['auth'] == 1){ echo("'".$_COOKIE['infos_user']['Mail']."'"); } ?> required>
                    </td>
                </tr>

                <tr>
                    <td>
                    <label for="password"> &nbsp Mot de Passe :</label>
                    </td>
                    <td>
                    <input type="password" name="Password" id="Password" required>
                    </td>
                </tr>

                <tr><td></td></tr>

                <tr>
                    <td>
                    &nbsp
                    <input type="submit" value="S'inscrire">
                    </td>
                </tr>

            </table>
        </form>

        <?php 

           }   
            else{
        
        ?>

                <h4> Pour vous inscrire à l'évènement, sélectionnez une option : </h4>
                <div class="col-4" id="divBoutonNavbar">
                    <a class="btn btn-outline-success" type="button" href="connexionUser.php" id="BoutonConnexionAcceuil1">Connexion</a>
                    <a class="btn btn-outline-secondary" type="button" href="InscriptionUser.php" id="BoutonConnexionAcceuil2">Inscription</a>
                </div>

        <?php

            }
        }
    

        ?>


    </body>
</html>