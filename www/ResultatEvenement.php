
<?php

include("include/ConnexionBDD.php");

include("include/fonctions.php");

// GESTION DE LA SESSION
include("include/session_cookie.php");

?>

<!DOCTYPE html>
<html>
    <head>
        <title>projet</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/LISTEevenements.css">
        <link rel="stylesheet" href="css/style.css">
        
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    </head>

    <body>
        <?php
            include("include/EnTete.php");
        ?>
        <div class="container">
            <div class="row">
            <div class="col-lg-10 mx-auto mb-4">
                <div class="section-title text-center "><br>
                    <h2 class="top-c-sep"> Résultats des évènements : </h2>
                </div>
            </div>
            </div>

            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <div class="career-search mb-60">
            <!-- Espace de recherche -->
                <form action="ResultatEvenement_post.php?Post=true" class="career-form mb-60" method="POST">
                    <div class="row">
                        <div class="col-md my-3">
                            <select class="form-select" aria-label="Default select example" name="IDEvenement">
                                <?php

                                    $req_type_evenement = $bdd->query('SELECT * FROM evenement ORDER BY NomEvenement');

                                    ?>

                                        <option value='' selected> Sélectionner l'évènement </option>

                                    <?php

                                    while ($donnees_type = $req_type_evenement->fetch())
                                    {
                                        ?>     
                                            <option value="<?php echo $donnees_type['IDevenement']; ?>"> <?php echo $donnees_type['NomEvenement']; ?></option>

                                        <?php
                                    }       

                                ?>
                            </select>
                        </div>

                        <div class="col-md my-3">
                            <button type="submit" class="btn btn-primary"> Afficher les résulats </button>
                        </div>
                        </div>      
                    </div>
                </form>
                    </div>
                <!-- Fin espace de recherche -->
            </div>

            <br><br>

            <div class="row">

                <!-- <iframe src="uploads/7d16af3f09cc3cb897081e617dd5533b.pdf" frameborder="0" id="PDFResultat"></iframe> -->
                <?php
                
                
                if(isset($_GET['ID']) && $_GET['ID'] != ""){

                    $req_info_resultat = "SELECT * FROM resultat WHERE IDevenement = ". $_GET['ID'];
                    $res_info_resultat = $bdd -> query($req_info_resultat); 
    
                    if($res_info_resultat -> rowCount() < 1){
    
                        
                        echo ("<p> Les résultats n'ont pas encore été transmis. </p>");
    
                    }
                    else{

                        
                    $info_resultat = $res_info_resultat->fetch();

                    //var_dump($info_resultat);

                    $location = "uploads/";
                    $location .= $info_resultat['NomFichier'];

                    echo("<iframe src='". $location ."' frameborder='0' id='PDFResultat'></iframe>");

                    }

                }

                ?>

            </div>
    
    </div>

    </body>
</html>