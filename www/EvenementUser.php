<?php

include("include/ConnexionBDD.php");

include("include/fonctions.php");

// GESTION DE LA SESSION
include("include/session_cookie.php");

?>

<!DOCTYPE html>
<html>
    <head>
        <title>projet</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>



<body id="body">
    <?php
        include("include/EnTete.php");
        $months = array('Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre');

        if(isset($_GET['msg']) && $_GET['msg'] == 1) { echo("<br><br> L'évènement à bien été supprimé."); }
        if(isset($_GET['msg']) && $_GET['msg'] == 2) { echo("<br><br> Il existe déjà un document résultat pour cette évènement."); }
        if(isset($_GET['msg']) && $_GET['msg'] == 3) { echo("<br><br> Le document résultat à bien été ajouté."); }
        if(isset($_GET['msg']) && $_GET['msg'] == 4) { echo("<br><br> L'extension du fichier n'est pas correcte, le document doit être un pdf."); }
        if(isset($_GET['msg']) && $_GET['msg'] == 5) { echo("<br><br> L'évènement à bien été supprimer, vous n'y participer plus."); }


    // L'utilisateur doit choisir choisir la nature des évènement auquels il veut accéder (partcipant ou organisateur)
    if(!isset($_GET['Eve'])){


    ?>
    

        <br>
        <div class="container" id="container">
            <div class="row">
                <div class="col"> <a href="EvenementUser.php?Eve=1" class="btn btn-primary"> Afficher les évènements que vous avez créé</a> </div>
                <div class="col"> <a href="EvenementUser.php?Eve=2" class="btn btn-primary"> Afficher les évènements auquels vous participez</a></div>
            </div>
        </div>

        <?php

    }

    ////////// Affichage partie organisateur ( évènement créé par l'utilisateur)
    if(isset($_GET['Eve']) && $_GET['Eve'] == 1){

        ?>

        <div class="container" id="container">
            <div class="row">

                <?php
                    if(isset($_SESSION['auth']) && $_SESSION['auth']==true){

                        $req_evenement = "SELECT evenement.IDevenement, evenement.NomEvenement, evenement.LieuEvenement, evenement.TypeEvenement, evenement.DateEvenement, statusevenementutilisateur.Statut FROM evenement, utilisateur, statusevenementutilisateur 
                        WHERE statusevenementutilisateur.IDutilisateur = utilisateur.IDUtilisateur AND evenement.IDevenement = statusevenementutilisateur.IDevenement AND mail = '". $_COOKIE['infos_user']['Mail']. "' AND statusevenementutilisateur.Statut = 1";

                        // var_dump($req_evenement);
                        // var_dump($_COOKIE['infos_user']);

                        $info_sql = $bdd->query($req_evenement);
                        //$info_evenement = $info_sql->fetch();
                        //var_dump($info_evenement);

                        if($info_sql -> rowCount() > 0){

                            $compt = 1;
                            $Liste_IDevenement = "";
                        
                            //var_dump($info_evenement);
                            echo("<div class='col align-self-center'>
                                <br><h2> Vos évènements créés : </h2><br>
                            

                            <div class='w-100'></div>
                            
                            <div class='filter-result'>

                                <div class='job-box d-md-flex align-items-center'>
                            ");
                            
                            while ($info_evenement = $info_sql->fetch()){

                                
                                    $Date = explode('-',$info_evenement['DateEvenement']);
                                    $Annee = $Date[0];
                                    $Mois = $months[$Date[1]-1];
                                    $Jour = $Date[2];
                                    $url_inscription = "DeleteEvenement.php?ID=" . $info_evenement['IDevenement'];
                                    
                                    //var_dump($info_evenement['IDevenement']);
                                    $ID = (string)$info_evenement['IDevenement'];
                                    $Liste_IDevenement = $Liste_IDevenement . "/" . $ID;
                                    //var_dump($Liste_IDevenement);

                ?>
                
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">  
                                        <h5 class="card-title center"> <?php echo($info_evenement['NomEvenement']); ?> </h5><br>
                                        <p class="card-text"> <?php echo(" <u>Date</u> : ". $Jour . " " . $Mois . " " . $Annee . "."); ?> </p>
                                        <p class="card-text"> <?php echo(" <u>Lieu</u> : ". $info_evenement['LieuEvenement'] . "."); ?> </p>
                                        <p class="card-text"> <?php echo(" <u>Sport</u> : ". $info_evenement['TypeEvenement'] . "."); ?> </p>
                                        <hr>
                                            <div class="center">

                                                <?php //include("upload.php");
                                                    $Chemin_Post = "Post_ResultatEvenement.php?IDEve=" . $info_evenement['IDevenement']; //action= <?php echo( "'". $Chemin_Post . "'")  //action="Post_ResultatEvenement.php"
                                                    $NameSubmit =  $info_evenement['IDevenement']; // name= <?php echo( "'". $NameSubmit . "'") 
                                                ?>

                                                <form method="POST" enctype="multipart/form-data" action= <?php echo( "'". $Chemin_Post . "'") ?>>
                                                        <input type="file" name="pdf" id="fichier" accept=".pdf">
                                                        <br><br>
                                                        
                                                        <input type="submit" class="uploadfile" value="Envoyer les résultats">

                                                        <!-- 
                                                        <label for="resultats" class="voirResultats">Voir les résultats</label>
                                                        <button id="resultats" onclick=voirResultat() hidden></button> -->
                                                        <br><br>
                                                </form>
                                                <a href= <?php echo("'". $url_inscription ."'"); ?> class="btn btn-primary">Supprimer l'événement</a>
                                            </div>
                                    </div>
                                </div>
                <?php
                        $compt++;
                        }
                    }
                    else{                      
                        echo("<div class='col align-self-center'>
                        <br><br>
                    

                    <div class='w-100'></div>
                    
                    <div class='filter-result'>

                        <div class='job-box d-md-flex align-items-center'>

                        <p> Vous n'avez pas créé d'évènement.</p>
                    
                        
                    ");
                    }
                ?>
                    </div>
                    </div>   
                    </div>

                <?php
                }
                ?>   
            </div>
        </div>

        <?php
    }

    ////////// Affichage partie participant (évènement auquels l'utilisateur c'est inscrit)
    if(isset($_GET['Eve']) && $_GET['Eve'] == 2){
    
        ?>
    
    <div class="container" id="container">
            <div class="row">

                <?php
                    if(isset($_SESSION['auth']) && $_SESSION['auth']==true){

                        $req_evenement = "SELECT evenement.IDevenement, evenement.NomEvenement, evenement.LieuEvenement, evenement.TypeEvenement, evenement.DateEvenement, statusevenementutilisateur.Statut FROM evenement, utilisateur, statusevenementutilisateur 
                        WHERE statusevenementutilisateur.IDutilisateur = utilisateur.IDUtilisateur AND evenement.IDevenement = statusevenementutilisateur.IDevenement AND mail = '". $_COOKIE['infos_user']['Mail'] . "' AND statusevenementutilisateur.Statut = 2";

                        // var_dump($req_evenement);
                        // var_dump($_COOKIE['infos_user']);

                        $info_sql = $bdd->query($req_evenement);
                        //$info_evenement = $info_sql->fetch();
                        //var_dump($info_evenement);

                        if($info_sql -> rowCount() > 0){

                            //var_dump($info_evenement);
                            echo("<div class='col align-self-center'>
                                <br><h2> Les évènements auquels vous êtes inscrit : </h2><br>
                            

                            <div class='w-100'></div>
                            
                            <div class='filter-result'>

                                <div class='job-box d-md-flex align-items-center'>
                            ");
                            
                            while ($info_evenement = $info_sql->fetch()){
                                
                                    $Date = explode('-',$info_evenement['DateEvenement']);
                                    $Annee = $Date[0];
                                    $Mois = $months[$Date[1]-1];
                                    $Jour = $Date[2];
                                    $url_ = "DeleteParticipation.php?ID=" . $info_evenement['IDevenement'];
                                    $url_ .= "&M=";
                                    $url_ .= $_COOKIE['infos_user']['Mail'];
                                    

                ?>
                
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">  
                                        <h5 class="card-title center"> <?php echo($info_evenement['NomEvenement']); ?> </h5><br>
                                        <p class="card-text"> <?php echo(" <u>Date</u> : ". $Jour . " " . $Mois . " " . $Annee . "."); ?> </p>
                                        <p class="card-text"> <?php echo(" <u>Lieu</u> : ". $info_evenement['LieuEvenement'] . "."); ?> </p>
                                        <p class="card-text"> <?php echo(" <u>Sport</u> : ". $info_evenement['TypeEvenement'] . "."); ?> </p>
                                        <hr>
                                            <div class="center">

                                                <?php //include("upload.php");
                                                    $Chemin_Post = "Post_ResultatEvenement.php?IDEve=" . $info_evenement['IDevenement']; //action= <?php echo( "'". $Chemin_Post . "'")  //action="Post_ResultatEvenement.php"
                                                    $NameSubmit =  $info_evenement['IDevenement']; // name= <?php echo( "'". $NameSubmit . "'") 
                                                ?>

                                                <a href= <?php echo("'". $url_ ."'"); ?> class="btn btn-primary">Annuler votre participation</a>
                                            </div>
                                    </div>
                                </div> 
                <?php
                        }
                    }
                    
                    else{                      
                        echo("<div class='col align-self-center'>
                        <br><br>
                    

                    <div class='w-100'></div>
                    
                    <div class='filter-result'>

                        <div class='job-box d-md-flex align-items-center'>

                        <p>Vous n'êtes inscrit à aucun évènement.</p>
                    
                        
                    ");
                    }
                ?>
                    </div>
                    </div>   
                    </div>

                <?php
                }
                ?>   
            </div>
        </div>

    <?php
    }
    ?>
    
</body>
</html>