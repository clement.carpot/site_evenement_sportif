<?php

include("include/ConnexionBDD.php");

include("include/fonctions.php");

// GESTION DE LA SESSION
include("include/session_cookie.php");

?>

<!DOCTYPE html>
<html>
    <head>
        <title>projet</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/style.css">
    </head>



    <body id="body">
    
        <?php
            include("include/EnTete.php");
        ?>
        
        <br>
        <center>
            <img src="image/sport17.jpg" class="logo_Sport17" alt="logo_Sport17">
        </center>
        
        <br>
        <div class="container" id="container">
            
            <!-- <h1 id="NomAssosciationAcceuil">Sport 17</h1>   -->

            <!-- <h4 id="SloganAssosciationAcceuil">Venez faire du sport comme vous êtes ! </h4> -->

            <!-- <div class="row right">
                <div class="col-lg">
                    <iframe src="https://feed.mikle.com/widget/v2/154571/?preloader-text=Loading" height="533px" width="450px" class="fw-iframe" scrolling="no" frameborder="0"></iframe>    
                </div> -->

            </div>
            <div class="row">
                <div class="col-lg">
                    <a href="ListeEvenement.php">
                        <img id="ImageAcceuil" src="./Image/RechercheEvenement.png" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg">
                    <a href="ResultatEvenement.php">
                        <img id="ImageAcceuil" src="./Image/ImageResultat.png" class="img-fluid">
                    </a>
                </div>
                <div class="col-lg">
                    <a href="AjoutEvenement.php">
                        <img id="ImageAcceuil" src="./Image/AjoutEvenement.png" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </body>
</html>