<?php

session_start();
session_unset();
session_destroy();

//connexion
include ("include/ConnexionBDD.php");


if (isset ($_POST['log']) && $_POST['log'] == "SE CONNECTER")
{   
    $mail = addslashes($_POST['mail_user']);

    $req_mail = "SELECT Mail FROM utilisateur WHERE Mail = '".$mail."'";

    $res_mail = $bdd -> query($req_mail);

    //var_dump($req_login);

    $num = $res_mail -> rowCount();
    
    if ($num == 1) // le login a été trouvé dans la table utilisateurs
    {   
        // on vérifie alors si le mot de passe est bon
        $mpasse = addslashes($_POST['mdp_user']);

        $req_infos = "SELECT * FROM utilisateur WHERE Mail ='" . $mail . "'";
        $res_infos = $bdd -> query($req_infos);
        $infos_user = $res_infos -> fetch();

        if(password_verify($mpasse, $infos_user['MotDePasse'])){

            session_start();
            $auth = true;

            // on enregistre les paramètres de connexion en session
            $_SESSION['auth'] = $auth;
            $_SESSION['infos_user'] = $infos_user;
            
            // on insère un cookie valable 12h
            $duree_cookie = 3600*12;

            // car en hébergement mutualisé, on ne maîtrise pas le temps de session
            setcookie("auth", $auth, time() + $duree_cookie);
            
            // on stocke les infos concernant le user ($infos_user est un array associatif)
            foreach ($infos_user as $k => $v)
            {
                setcookie("infos_user[".$k."]", $v, time() + $duree_cookie);
            }
            
            header ("Location: Index.php");
        }
        else // le mot de passe est incorrect
        {   
            $message = header ("Location: connexionUser.php?msg=1");
        }
    }
    elseif ($num == 0) // email non existant dans la table utilisateurs
    {   
        $message = header ("Location: connexionUser.php?msg=2");
    }
}

$tab_messages = array (
	1  => '<div class="alert alert-warning">'."Le couple identifiant/mot de passe est incorrect !".'</div>',
	2  => '<div class="alert alert-danger">'."Cet identifiant n'existe pas.".'</div>',
);

?>

<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Connexion</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="css/sb-admin-2.min.css" rel="stylesheet">
    <link href="css/general.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <div class="row align-item-center">
                            <div class="col">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Bienvenue !</h1>
                                        <?php
                                        if (isset($_GET['msg']) && $_GET['msg'] != '')
                                            echo $tab_messages[$_GET['msg']]."\n";
                                        ?>
                                    </div>

                                    <form method="POST" class="user" action="connexionUser.php">
                                        <div class="form-group">
                                            <input type="email" name="mail_user" class="form-control form-control-user" required placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="mdp_user" required class="form-control form-control-user" placeholder="Mot de passe">
                                        </div>
                                        <input type="submit" name="log" class="btn btn-primary btn-bleu btn-user btn-block" value="SE CONNECTER">
                                    </form>
                                    
                                    <hr>
                                    <div class="text-center">
                                        <a href="InscriptionUser.php">
                                            Inscription
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>